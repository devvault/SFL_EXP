class CfgPatches
{
	class SFL_EXP
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"DZ_Data","DZ_Sounds_Effects","DZ_Scripts","SearchForLoot"};
	};
};
class CfgMods
{
	class SFL_EXP
	{
		dir = "SFL_EXP";
		picture = "";
		action = "";
		hideName = 0;
		hidePicture = 0;
		name = "SFL_EXP";
		credits = "Hunterz";
		author = "DayZ Russia";
		authorID = "0";
		version = "1.0";
		extra = 0;
		type = "mod";
		class defs
		{
			class gameScriptModule
			{
				value = "";
				files[] = {"SFL_EXP/Common","SFL_EXP/Scripts/3_Game"};
			};
			class worldScriptModule
			{
				value = "";
				files[] = {"SFL_EXP/Common","SFL_EXP/Scripts/4_World"};
			};
			class missionScriptModule
			{
				value = "";
				files[] = {"SFL_EXP/Common","SFL_EXP/Scripts/5_Mission"};
			};
		};
	};
};
class CfgSoundShaders
{
	class base_midShot_SoundShader;
	class SearchingForLoot_SoundShader5: base_midShot_SoundShader
	{
		samples[] = {{"SFL_EXP\sounds\collapse1",1}};
		volume=1;
		range=600;
		rangeCurve[]=		{
			{0,1},
			{100,0.95},
			{450,0.14399999},
			{600,0.34799999}
		};
	};
	class SearchingForLoot_SoundShader6: SearchingForLoot_SoundShader5
	{
		samples[] = {{"SFL_EXP\sounds\collapse1",1}};
	};
	class SearchingForLoot_SoundShader7: SearchingForLoot_SoundShader5
	{
		samples[] = {{"SFL_EXP\sounds\collapse1",1}};
	};
	class SearchingForLoot_SoundShader8: SearchingForLoot_SoundShader5
	{
		samples[] = {{"SFL_EXP\sounds\collapse1",1}};
	};
};
class CfgSoundSets
{
	class baseCharacter_SoundSet;
	class SearchingForLoot_SoundSet1: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader1"};
	};
	class SearchingForLoot_SoundSet2: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader2"};
	};
	class SearchingForLoot_SoundSet3: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader3"};
	};
	class SearchingForLoot_SoundSet4: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader4"};
	};
	class SearchingForLoot_SoundSet5: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader5"};
	};
	class SearchingForLoot_SoundSet6: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader6"};
	};
	class SearchingForLoot_SoundSet7: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader7"};
	};
	class SearchingForLoot_SoundSet8: baseCharacter_SoundSet
	{
		soundShaders[] = {"SearchingForLoot_SoundShader8"};
	};
};
