modded class ActionSearchForLootCB
{
	static ref NoiseParams 	m_NoisePar;
	static NoiseSystem		m_NoiseSystem;
	override void CreateActionComponent()
	{	
		//SFL_EXP_Config config = GetDayZGame().GetSFL_EXPConfig();
		
		//int searchtimeVar = config.SearchTimeVarianceSec;
		int searchtimeVar = 2;
		//int searchtime = config.SearchTimeSec - (Math.RandomInt(-1*searchtimeVar,searchtimeVar));
		int searchtime = 4;
		//searchtime = 3;
		m_ActionData.m_ActionComponent = new CAContinuousTime(searchtime);
	}
};

modded class ActionSearchForLoot
{
	override string GetText()
	{
		return "#STR_SFL_EXP_search";
	}
	
	override void OnStartServer(ActionData action_data )
	{	
		//start sound
		SFLConfig config = GetDayZGame().GetSearchForLootConfig();
		if (config.SoundEnabled)
		{
			action_data.m_Player.StartSearchSound();
			//COLLAPSE
			int rand = Math.RandomInt(0,15);
			//33% chance its rotten
			if ( rand > 10 )
			{
			
				action_data.m_Player.StartCollapseSound();
				//m_NoiseSystem.AddNoiseTarget( GetPosition(), UPDATE_TICK_RATE, m_NoisePar);
				
				// add explosion noise
				if ( GetGame().IsServer() )
				{
					NoiseParams npar = new NoiseParams();
					npar.LoadFromPath("cfgVehicles AlarmClock_ColorBase NoiseAlarmClock");
					
					GetGame().GetNoiseSystem().AddNoiseTarget(action_data.m_Player.GetPosition(), 41, npar, 7);
				}
				//COLLAPSE
			}
		}
		
	}
	
	override void OnFinishProgressServer( ActionData action_data )
	{	
		
		action_data.m_Player.ResetCollapseSound();
		action_data.m_Player.ResetSearchSound();
		Object targetObject = action_data.m_Target.GetObject();
		string targetBuildingName = Object.Cast(action_data.m_Target.GetParent()).ClassName();
		vector pos = action_data.m_Player.ModelToWorld("0 0 0.2");
		SFLConfig config = GetDayZGame().GetSearchForLootConfig();
		string buildingCategory;
		SFLLogger.Debug("Player" + action_data.m_Player.GetIdentity().GetName() + " searched building " + targetBuildingName + " category " + GetBuildingCategory(targetBuildingName));
		int initialcooldown = config.InitialCooldown;
		int xpgain = Math.WrapInt(config.XPGain,1,10);
		if (initialcooldown == 0)
		{
			initialcooldown = 400;
		}
		if (GetRandomItem(GetBuildingCategory(targetBuildingName)) != "nothing")
		{
			// more random and also according to online time of player
			float rndnum = Math.RandomFloat(0,100);
			float rarita;
			if (GetRarity(targetBuildingName) == 0)
			{
				rarita = config.Rarity;	
			}
			else
			{
				rarita = GetRarity(targetBuildingName);
			}
			action_data.m_Player.StatUpdate( "sfl_objects_searched", xpgain );
			action_data.m_Player.StatSyncToClient();
			// action_data.m_Player.StatGet("sfl_objects_searched") 
			if (rarita > rndnum &&  action_data.m_Player.GetTimeCreated()+initialcooldown < GetGame().GetTime()/1000)
			{
				if (!config.DisableNotifications && action_data.m_Player)
				{
					NotificationSystem.SendNotificationToPlayerExtended(action_data.m_Player, 2, "#STR_SFL_EXP_Searching", "#STR_SFL_EXP_Found", "set:dayz_gui image:tutorials");
				}
				ItemBase item = ItemBase.Cast(GetGame().CreateObject(GetRandomItem(GetBuildingCategory(targetBuildingName)), pos));
				//ItemBase item = ItemBase.Cast(CEApi.SpawnEntity(GetRandomItem(GetBuildingCategory(targetBuildingName)), pos, 1, 1));
				//Zedmag
				//added for organic foods.. 
				Edible_Base food;
				Class.CastTo(food,item);
				if ( food && food.HasFoodStage()) //possible NPE ?
				{
					int rand = Math.RandomInt(0,15);
					Edible_Base item_EB = Edible_Base.Cast(item);
					//33% chance its rotten
					if ( rand > 10 )
					{
						item_EB.ChangeFoodStage( FoodStageType.ROTTEN );
					}
					//33% chance its dried
					else if ( rand <=10 && rand >= 6 )
					{
						item_EB.ChangeFoodStage( FoodStageType.DRIED );
					}
					//if its less than 6 this its Raw
				}
				
				SetRandomQuantity(item);
				float maxcoef = Math.Clamp(config.MaxHealthCoef, 0.4, 1.0);
				
				Magazine_Base mag;	
        		if (item && !Class.CastTo(mag, item))
				{
					float rndhealth = Math.RandomFloat(item.GetMaxHealth("", "")*0.1, item.GetMaxHealth("", "")*maxcoef);
					item.SetHealth("", "", rndhealth);
				}
				//random damage of items
				if (item)
				{
					item.SetLifetime(900);
				}
				
				//TakeItemInHands
				if(item)
				{
					EntityAI ntarget = EntityAI.Cast(item);
					action_data.m_Player.TakeEntityToHandsImpl(InventoryMode.PREDICTIVE, ntarget);
				}
				//TakeItemInHands
				
			}
			//Zedmag
			//added 'else' for NOT meeting config.Rarity condition
			else 
			{
				//Zedmag
				//could add to config for custom msg.. 
				if (!config.DisableNotifications && action_data.m_Player)
				{
					NotificationSystem.SendNotificationToPlayerExtended(action_data.m_Player, 2, "#STR_SFL_EXP_Searching", "#STR_SFL_EXP_Empty", "set:dayz_gui image:tutorials");
				}
				
			}
			
		}
		
		
		
		
	}
	
	
};