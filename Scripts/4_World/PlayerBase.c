modded class PlayerBase
{
	//protected bool m_SFSearchSound;
	protected bool m_SFCollapseSound;

	void PlayerBase()
	{
		RegisterNetSyncVariableBool("m_SFCollapseSound");
	}
	
	override void OnVariablesSynchronized()
	{
		super.OnVariablesSynchronized();		
		if (m_SFCollapseSound)
		{
			CollapseItemSoundPlay();
		}
				
    }
	
	void CollapseItemSoundPlay()
	{
        int soundVariation = Math.RandomInt(5,8);
		EffectSound m_CollapseSound = SEffectManager.PlaySoundOnObject( "SearchingForLoot_SoundSet"+soundVariation, this );
		//Print ("SOUND WHISTLE");
        //m_WhistleSound.SetMaxVolume(3.0);
		m_CollapseSound.SetSoundAutodestroy( true );
		ResetCollapseSound();
	}
	
	void StartCollapseSound()
	{
		m_SFCollapseSound = true;
		SetSynchDirty();	
	}
	
	void ResetCollapseSound()
	{
		m_SFCollapseSound = false;
		SetSynchDirty();
	}
	
}