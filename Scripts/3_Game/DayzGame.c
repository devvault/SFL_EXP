modded class DayZGame
{	
	protected ref SFL_EXP_Config m_SFL_EXPConfig;

	
    void SetSFL_EXPConfig(SFL_EXP_Config config)
    {
        m_SFL_EXPConfig = config;
    }

    SFL_EXP_Config GetSFL_EXPConfig()
    {
		return m_SFL_EXPConfig;
    }
};