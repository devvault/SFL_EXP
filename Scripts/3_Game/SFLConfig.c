class SFL_EXP_Config
{
       bool SearchAlertsAI;
		int SearchTimeSec;
		int SearchTimeVarianceSec;
		//configData.BareHandsCutOnLowStamina = 1;
		//configData.BareHandsCutChance = 5;
		//configData.GlovesDamagePerSearch = 0.25;
		int DrainStaminaPerSearch;
		bool TakeItemInHands;
		int JunkCollapseChance;
		bool JunkCollapseAlertsAI;
		//configData.JunkCollapseMoreWithLowStamina = 1;
		//configData.JunkCollapseAlwaysWithFoundItem = 1;
		//configData.LearnToAvoidCollapse = 1;
		//configData.LearnToAvoidCollapseExpPerSearch = 0.25;
		//configData.LearnToAvoidCollapseSlowerExpAfter = 85;

}

class SFL_EXPConfig
{

    protected const static string m_ConfigRoot = "$profile:\\DZR\\SFL_EXP\\";

    static void LoadConfig(string configName, out SFL_EXP_Config configData)
    {
        string configPath = m_ConfigRoot + configName;

        if (!FileExist(configPath))
        {
            SFLLogger.Log("'" + configName + "' does not exist, creating default config");
            CreateDefaultConfig(configData);
            SaveConfig(configName, configData);
            return;
        }

        JsonFileLoader<SFL_EXP_Config>.JsonLoadFile(configPath, configData);
        SFLLogger.Log("'" + configName + "' found, loading existing config");

    }

    protected static void SaveConfig(string configName, SFL_EXP_Config configData)
    {
        string configPath = m_ConfigRoot + configName;

        if (!FileExist(m_ConfigRoot))
        {
            SFLLogger.Log("'" + m_ConfigRoot + "' does not exist, creating directory");
            MakeDirectory(m_ConfigRoot);
        }
        JsonFileLoader<SFL_EXP_Config>.JsonSaveFile(configPath, configData);
    }
	
	static void UpgradeConfig(string configName, out SFL_EXP_Config configData)
    {
        string configPath = m_ConfigRoot + configName;

        if (!FileExist(configPath))
        {
            SFLLogger.Log("'"  + configName + "' does not exist, creating default config");
            CreateDefaultConfig(configData);
            SaveConfig(configName, configData);
            return;
        }
		JsonFileLoader<SFL_EXP_Config>.JsonLoadFile(configPath, configData);
		
		//upgrades...
		/*
		if (!configData.MaxHealthCoef)
		{
			configData.MaxHealthCoef = 1.0;	
		}
		
		if (!configData.XPGain)
		{
			configData.XPGain = 1;
		}
			
        JsonFileLoader<SFL_EXP_Config>.JsonSaveFile(configPath, configData);
		*/
    }

    protected static void CreateDefaultConfig(out SFL_EXP_Config configData)
    {
        configData = new SFL_EXP_Config();
		configData.SearchAlertsAI = 1;
		configData.SearchTimeSec = 3;
		configData.SearchTimeVarianceSec = 2;
		//configData.BareHandsCutOnLowStamina = 1;
		//configData.BareHandsCutChance = 5;
		//configData.GlovesDamagePerSearch = 0.25;
		configData.DrainStaminaPerSearch = 20;
		configData.TakeItemInHands = 1;
		configData.JunkCollapseChance = 95;
		configData.JunkCollapseAlertsAI = 1;
		//configData.JunkCollapseMoreWithLowStamina = 1;
		//configData.JunkCollapseAlwaysWithFoundItem = 1;
		//configData.LearnToAvoidCollapse = 1;
		//configData.LearnToAvoidCollapseExpPerSearch = 0.25;
		//configData.LearnToAvoidCollapseSlowerExpAfter = 85;
	}
}
